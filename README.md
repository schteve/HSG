## three
Enumerates microstates of a simple 2D model of a hard sphere gas via recursion.

In a H x H (or Hh x Hw) grid a 'large' 4 x 4 particle is placed with T 'small' 2 x 2 particles.

### Usage 
    three [ H | [Hh Hw] ] [ T ]

example output:

    $ bin/three 6 2
    
             1          1          1          1          0          0 
             1          1          1          1          0          0 
             1          1          1          1          0          0 
             1          1          1          1          0          0 
             0          0          0          0          0          0 
             0          0          0          0          0          0 
    
    
             9          5          9 
             5          0          5 
             9          5          9 
    
    
            27          6         27 
             6          0          6 
            27          6         27 
    
    $ 

## TODO
- [ ] Tidy up output
    - [x] Set uniform width
    - [ ] Plot
- [ ] Add overloaded operator(s)
    - [x] Output
    - [ ] Equality
- [x] Add command line options for H, T
    - [x] Add copy/move
- [x] Optimisations:
    - [x] Use the symmetry ==> quarter of computations
    - [x] Currently overcounting, which adds unnecessary work inc. factorial
- [x] Basic classes/functions

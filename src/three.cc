/* three.cc
 * 
 * Part of HSG, which examines entropic forces in a hard sphere gas
 * 
 * Copyright © 2017-2018 Steven Moseley <stevenmos@kolabnow.com>
 * Bonkers is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "grid.h"
#include "thrFunc.h"

int main(int argc, char **argv)
{
    // Argument validation
    if (argc != 3 && argc !=4) {
        Usage();
        return -1;
    }

    grid G;
    // Assume argc == 3, i.e. a square grid
    unsigned T = stoi(std::string(argv[2]));
    unsigned Hh = stoi(std::string(argv[1]));
    unsigned Hw = Hh;

    if (argc == 4) {
        Hw = T;
        T = stoi(std::string(argv[3]));
    }

    grid G2(Hh, Hw);
    G = std::move(G2);

    // Place a large article and print for context
    G.PlaceBig(0,0);  
    std::cout << std::endl << G << std::endl; 

    // Do recursion
    for (unsigned n = 1; n <= T; ++n) {
        BigRecurse(G, n);
    }

    return 0;
}

/* Write a function that takes a single long long and returns the no.
 * of factors of two in the number
 */
#include <iostream>
#include <cmath>

long long recurse(long long val);

inline long long log2(long long num)
{
    return (log(num)/log(2));
}

inline long long twos(long long val) 
{
    return log2(val/recurse(val));
}

int main()
{
    long long i = 0;

    std::cout << "Enter an integer:" << std::endl;
    std::cin >> i;

    std::cout << twos(i) << std::endl;

    return 0;
}

long long recurse(long long val)
{
    if (val % 2 == 0 && val != 0) {
        return (0 + recurse(val/2)); // even
    } else if (val == 0) { 
        return 0;                    // zero
    } else {
        return val; // odd, log(val/val) = 0
    }
}

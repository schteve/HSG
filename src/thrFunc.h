// thrFunc.h

#include <iostream>
#include "grid.h"

typedef unsigned un;
typedef const un coun;

grid GetGrid(char**, un&);
void Usage();
void BigRecurse(grid&, coun&);
void SmaRecurse(grid&, un, un&, coun&, coun&);

/* Write a function, printWithCommas, that takes a single non-
 * negative long integer argument and displays it with commas 
 * inserted in the conventional way, e.g. 12,045,670, or 1
 */
#include <iostream>
#include <cmath>

void printWithCommas(const long long &u);
void recurse(long long u, long long power);

inline 
long long getPower(const long long &u)
{
    long long col = log10(u);
    long long power = 0;

    while ((2 + power) < col) {
        power += 3;
    }

    return power;
}

inline 
int getTemp(const long long &u, const long long &power)
{
    return u/pow(10,power);
}

inline 
long long decVal(const long long &u, const int &temp, const long long &power)
{
    return (u - temp * pow(10, power));
}

inline 
long long decPower(const long long &power)
{
    return power - 3;
}

inline 
void pad(const long long &temp)
{
    if (temp < 100) {
        std::cout << "0";

        if (temp < 10) {
           std::cout << "0";
        }
    }
}

inline 
void comma(const long long &power)
{
    if (power != 0) {
       std::cout << ",";
    }
}

int main()
{
    long long u = -1;

    while (u  < 0) {
        std::cin >> u;
    }

    printWithCommas(u);

    return 0;
}

void printWithCommas(const long long &u)
{
    long long power = getPower(u);
    int temp = getTemp(u, power); 

    std::cout << temp << ",";
    recurse(decVal(u, temp, power), decPower(power));
}

void recurse(long long u, long long power)
{
    if (power >= 0) {
        int temp = getTemp(u, power);

        pad(temp);
        std::cout << temp;
        comma(power);

        recurse(decVal(u, temp, power), decPower(power));
    } else {
        std::cout << std::endl;
    }
}

// thrFunc.cc Externally defined functions for thrFunc.h

#include "thrFunc.h"

void Usage()
{
    std::cerr << "three [ H | [Hh Hw] ] [ T ]" << std::endl;
}

void BigRecurse(grid &g, const unsigned &T)
{
    unsigned a = 4, b = a;  // size of large paticle
    // The results grid is smaller than the real grid:
    unsigned resH = g.height() - (b-1), resW = g.width() - (a-1);
    grid results(resH, resW);

    // Get halfway points to only find one quadrant's worth
    unsigned resHhalf = resH/2, resWhalf = resW/2;
    if (resH % 2) 
    { resHhalf += 1; }
    if (resW % 2) 
    { resWhalf += 1; }

    // Do one quadrant's computations
    for (unsigned y = 0; y < resHhalf; ++y) {
        for (unsigned x = 0; x < resWhalf; ++x) {
            unsigned count = 0; // count of successful branches
            g.PlaceBig(x,y); // for this location
            SmaRecurse(g, T, count, 0, 0);
            *(results.xy(x,y)) += count;
        }
    } 

    // Copy into other quadrants
    for (unsigned y = 0; y < resHhalf; ++y) {
        for (unsigned x = 0; x < resWhalf; ++x) {
            *(results.xy(resW-(x+1),y)) = *(results.xy(x,y));
        }
    }

    for (unsigned x = 0; x < resW; ++x) {
        for (unsigned y = 0; y < resHhalf; ++y) {
            *(results.xy(x, resH-(y+1))) = *(results.xy(x,y));
        }
    }

    std::cout << results << std::endl;
}

void SmaRecurse(grid &g, unsigned T, unsigned &count, 
        const unsigned &X, const unsigned &Y)
{
    if (T > 0) { // If there are small particles to place
        unsigned x = X, y = Y;
        while (y < g.height() - (2-1)) { // while a particle 
            while (x < g.width() - (2-1)) { // will fit
                if (g.PlaceSma(x, y)) { // if clear
                    SmaRecurse(g, T-1, count, x, y);//recurse
                    g.UnPlaceSma(x, y); // remove small 
                } // particle when finished with it
                ++x;
            }
            x = 0; // rest x c.f. carriage return
            ++y;
        }
    } else {
        ++count; // For successful branches
    }
}

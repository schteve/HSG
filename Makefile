# MAKEFILE for S7
# 
# Modified from the original by David MacKay 
# http://www.inference.org.uk/mackay/

# if you want the debugger kdbg to work nicely, REMOVE the -O2 flag
# if you want to get all warnings enabled, INCLUDE the -O2 flag

CFLAGS = $(INCDIRS)  \
	-pedantic -g -O2\
	-Wall -Wconversion\
	-Wformat  -Wshadow\
	-Wpointer-arith -Wcast-qual -Wwrite-strings\
	-D__USE_FIXED_PROTOTYPES__

LIBS = -l stdc++ -lm

CXX = g++

threeobjects = bin/three.o bin/thrFunc.o bin/grid.o

three: $(threeobjects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/three $(threeobjects)

bin/three.o: src/three.cc src/thrFunc.cc src/thrFunc.h src/grid.cc src/grid.h
	$(CXX) $(CFLAGS) $(LIBS) -c -o bin/three.o src/three.cc

bin/thrFunc.o: src/thrFunc.cc src/thrFunc.h
	$(CXX) $(CFLAGS) $(LIBS) -c -o bin/thrFunc.o src/thrFunc.cc

bin/grid.o: src/grid.cc src/grid.h
	$(CXX) $(CFLAGS) $(LIBS) -c -o bin/grid.o src/grid.cc 

One: src/One.cc
	$(CXX) $(CFLAGS) $(LIBS) -o bin/One src/One.cc

two: src/two.cc
	$(CXX) $(CFLAGS) $(LIBS) -o bin/two src/two.cc

.PHONY: clean
clean:
	rm bin/*.o
